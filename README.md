Install
-------
Use `python3.5+` and `tensorflow==1.13.0`

Contribute!
-----------
This is collaborative project.
Please, see the forks of this repository and consider improving work also from other forked repositories.

Please, create pull request to **master** branch if you want to merge back your changes.
Let's keep a **master** branch as stable as possible.
Use feature branches if you want to work on some feature.
